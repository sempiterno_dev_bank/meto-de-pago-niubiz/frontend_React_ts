export {};

declare global {
  interface Window {
    VisanetCheckout: {
      configure: (config: CheckoutConfig) => void;
      open: () => void;
    };
  }
}

interface CheckoutConfig {
  sessiontoken: string;
  channel: string;
  merchantid: string;
  purchasenumber: string;
  amount: string;
  expirationminutes: string;
  timeouturl: string;
  merchantlogo: string;
  formbuttoncolor: string;
  complete: (params: any) => void;
}