import { useState, useEffect } from "react";
import "./App.css";
import axios from "axios";

const App = () => {
  const [formulario, setFormulario] = useState(true);
  const [sessionKey, setSessionKey] = useState("");

  useEffect(() => {
    if (!formulario && sessionKey) {
      const script = document.createElement("script");
      script.src = "https://static-content-qas.vnforapps.com/v2/js/checkout.js";
      script.async = true;
      script.onload = () => configureCheckout(); // Configurar después de cargar el script
      document.body.appendChild(script);

      return () => {
        document.body.removeChild(script);
      };
    }
  }, [formulario, sessionKey]);

  const openForm = () => {
    if ((window as any).VisanetCheckout) {
      (window as any).VisanetCheckout.open();
    }
  };
  
  // Asegúrate de que la función de configuración también está correctamente tipada:
  const configureCheckout = () => {
    if ((window as any).VisanetCheckout) {
      (window as any).VisanetCheckout.configure({
        sessiontoken: sessionKey,
        channel: 'web',
        merchantid: '456879852',
        purchasenumber: '00000001',
        amount: '100.41',
        expirationminutes: '20',
        timeouturl: 'http://localhost:5173/',
        merchantlogo: 'img/comercio.png',
        formbuttoncolor: '#000000',
        complete: (params: any) => {
          console.log(JSON.stringify(params));
        }
      });
    }
  };

  const handleSession = async () => {
    try {
      const response = await axios.post("http://localhost:3001/niubiz", {
        email: "kennethmazuelosvargas.km@gmail.com",
        amount: "100.41",
        fechaCreacion: "2012-06-01",
      });
      setSessionKey(response.data);
      setFormulario(false);
    } catch (error) {
      console.error("Error al hacer la solicitud:", error);
    }
  };

  return (
    <>
      {formulario ? (
        <div style={{ maxWidth: "400px", width: "400px", height: "400px", background: "#ffff", borderRadius: "50px", padding: "10px" }}>
          <h1 style={{ color: "#000" }}>Pantalones</h1>
          <p style={{ color: "#000" }}><strong>Soles</strong> 100.41</p>
          <button type="button" onClick={handleSession}>
            Comprar
          </button>
        </div>
      ) : (
        <div>
          <button onClick={openForm} type="button">Pagar</button>
        </div>
      )}
    </>
  );
};

export default App;